/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.chense.kanbackend.websocket;

import in.chense.kanbackend.BoardFactory;
import in.chense.kanbackend.KanBoard;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.EncodeException;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

/**
 *
 * @author prasannakumar
 */
@ClientEndpoint
public class KanboardSocketClient {

    @OnOpen
    public void sendMsg(Session session) {
        try {
            String kanBoard = BoardFactory.getInstance().obtainBoard(null).getBoardJson();
            session.getBasicRemote().sendObject(kanBoard);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(KanboardSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KanboardSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EncodeException ex) {
            Logger.getLogger(KanboardSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void propogateChanges() {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
           String uri = "ws://localhost:8080/urbantravellers/SignalChange";
           container.connectToServer(KanboardSocketClient.class, URI.create(uri));
        } catch (DeploymentException ex) {
            Logger.getLogger(KanboardSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KanboardSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

angular.module('kanban', []).
        config(function($routeProvider) {
    $routeProvider.
    		when('/', {controller: IndexCtrlr, templateUrl: 'html/jobs.html'}).
            when('/create', {controller: JobCtrlr, templateUrl: 'html/create.html'}).
            otherwise({redirectTo: '/'});
});

function JobCtrlr($scope,$location) {
    $scope.createJob = function() {
        UIRemote.createJob($scope.job.name,$scope.job.desc);     
    	$location.path('/');
    };
}

function IndexCtrlr($scope,$location) {
	$scope.jobs = [];
	
	UIRemote.listJobs({
    	callback : function(str) {
    		var obj = JSON.parse(str);
    		for(job in obj.jobs) {
    			$scope.jobs.push(obj.jobs[job]);
    		}
    	}
    });
}
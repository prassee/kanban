/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.chense.kanbackend;

/**
 *
 * @author prasannakumar
 */
public enum Lane {

    TODO {
        
        @Override
        public String toString() {
            return "todo";
        }
    }, DOING {
        @Override
        public String toString() {
            return "doing";
        }
    }, DONE {
        @Override
        public String toString() {
            return "done";
        }
    }, ARCHIVE {
        @Override
        public String toString() {
            return "archive";
        }
    }
}

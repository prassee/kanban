/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.chense.remoting;

import com.google.gson.Gson;
import in.chense.kanbackend.BoardFactory;
import in.chense.kanbackend.BoardManager;
import in.chense.kanbackend.KanCard;
import in.chense.kanbackend.Lane;

/**
 *
 * @author prasannakumar
 */
public class BoardRemote {

    private static BoardManager boardManager = BoardFactory.getInstance().obtainBoard(null);
    private static Gson gson = new Gson();

    public void addCard(String lane, String card) {
        boardManager.addCardToBoard(Lane.valueOf(lane), gson.fromJson(card, KanCard.class));
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.chense.kanbackend;

import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author prasannakumar
 */
public class BoardTest {

    private KanCard card;
    private BoardManager boardManager;
    private KanbackEnd jsonrm = new KanbackEnd();

    @Before
    public void init() {

        card = new KanCard();
        card.setId(1);
        card.setName("go cycling");
        card.setOrder(1);

        KanLane todoLane = new KanLane();
        todoLane.setId(1);
        todoLane.setName("todo");

        KanLane doingLane = new KanLane();
        doingLane.setId(2);
        doingLane.setName("doing");

        KanLane doneLane = new KanLane();
        doneLane.setId(3);
        doneLane.setName("done");

        KanLane archiveLane = new KanLane();
        archiveLane.setId(4);
        archiveLane.setName("archive");

        KanBoard kanBoard = new KanBoard();
        KanLane lanes[] = new KanLane[4];

        lanes[0] = todoLane;
        lanes[1] = doingLane;
        lanes[2] = doneLane;
        lanes[3] = archiveLane;

        kanBoard.setLanes(lanes);
        kanBoard.setName("kanban board");
        boardManager = new BoardManager(kanBoard);
    }

    /**
     *
     */
    @Test
    public void testAddCard() {
        boardManager.addCardToBoard(Lane.TODO, card);
        card = new KanCard();
        card.setId(1);
        card.setName("go driving");
        card.setOrder(2);
        boardManager.addCardToBoard(Lane.TODO, card);
        assertEquals(boardManager.board.getLane(Lane.TODO).getCards().size(), 2);
    }

    @Test
    public void testMoveCard() {
        boardManager.addCardToBoard(Lane.TODO, card);
        card = new KanCard();
        card.setId(1);
        card.setName("go driving");
        card.setOrder(2);
        boardManager.addCardToBoard(Lane.TODO, card);

        boardManager.moveCardInBoard(Lane.TODO, Lane.DOING, card);
        assertEquals(boardManager.board.getLane(Lane.TODO).getCards().size(), 1);
        assertEquals(boardManager.board.getLane(Lane.DOING).getCards().size(), 1);
    }

    @After
    public void writeToFile() throws IOException {
        jsonrm.writeBoardToFile(boardManager.board, "/Users/prasannakumar/a.txt");
    }
}

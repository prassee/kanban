package in.chense.kanbackend;

public class KanBoard {

    private String name;
    private KanLane[] lanes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KanLane[] getLanes() {
        return lanes;
    }

    public void setLanes(KanLane[] lanes) {
        this.lanes = lanes;
    }

    public KanLane getLane(Lane lane) {
        KanLane line = new KanLane();
        switch (lane) {
            case TODO:
                line = this.lanes[0];
                break;
            case DOING:
                line = this.lanes[1];
                break;
            case DONE:
                line = this.lanes[2];
                break;
        }
        return line;
    }

    public KanLane getLane(String lane) {
        return getLane(Lane.valueOf(name));
    }
}

package in.chense.kanbackend;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.IOException;

public class BoardManager {

    protected KanBoard board;

    public BoardManager(KanBoard kanBoard) {
        board = kanBoard;
    }

    public BoardManager() {
    }

    public void addCardToBoard(Lane lane, KanCard card) {
        board.getLane(lane).getCards().add(card);
    }

    public void moveCardInBoard(Lane lane, Lane toLane, KanCard card) {
        board.getLane(lane).getCards().remove(card);
        board.getLane(toLane).getCards().add(card);
    }

    public String getBoardJson() {
        return new Gson().toJson(board);
    }

    public KanBoard getBoard() throws FileNotFoundException, IOException {
        return this.board;
    }

    public void setBoard(KanBoard board) {
        this.board = board;
    }
}

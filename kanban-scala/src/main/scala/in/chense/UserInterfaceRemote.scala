package in.chense

import scala.collection.mutable.ArrayBuffer

object UserInterfaceRemote {

  private val jobs: ArrayBuffer[Job] = ArrayBuffer[Job]()

  def createJob(name: String, desc: String): Unit = {
    jobs.+=:(Job(name, desc))
  }

  def listJobs(): String = {
    val resp = new StringBuffer
    resp.append("{\"jobs\":[")
    jobs.foreach(job => {
      resp.append("{\"name\":\"" + job.name + "\"},")
    })
    resp.append("{\"name\":\"John\"}")
    resp.append("]}")
    resp.toString()
  }

}
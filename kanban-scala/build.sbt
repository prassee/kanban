name := "kanban"

scalaVersion := "2.10.0"

seq(webSettings :_*)

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0.M5b" % "test"

libraryDependencies += "org.directwebremoting" % "dwr" % "2.0.3"

libraryDependencies += "commons-logging" % "commons-logging" % "1.1"

libraryDependencies += "org.eclipse.jetty" % "jetty-webapp" % "8.0.1.v20110908" % "container"
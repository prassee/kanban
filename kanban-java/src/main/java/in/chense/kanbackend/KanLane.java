package in.chense.kanbackend;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author prasannakumar
 */
public class KanLane {

    private String name;
    private int id;
    private List<KanCard> cards = new ArrayList<KanCard>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<KanCard> getCards() {
        return cards;
    }

    public void setCards(List<KanCard> cards) {
        this.cards = cards;
    }
}

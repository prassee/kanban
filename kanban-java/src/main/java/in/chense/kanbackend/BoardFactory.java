/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.chense.kanbackend;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author prasannakumar
 */
public class BoardFactory {

    private static BoardFactory boardFactory = new BoardFactory();

    private BoardFactory() {
    }

    public static BoardFactory getInstance() {
        return boardFactory;
    }

    public BoardManager obtainBoard(String boardName) {
        KanBoard board = null;
        try {
            KanbackEnd kanbackEnd = new KanbackEnd();
            board = kanbackEnd.readBoardFromFile(boardName);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BoardFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BoardFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new BoardManager(board);
    }
}

package in.chense.kanbackend;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class KanbackEnd {

    private static Gson gson = new Gson();

    public void writeBoardToFile(KanBoard json, String file) throws IOException {
        File db = new File(file);
        if (!db.exists()) {
            db.createNewFile();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(db, false));
        bufferedWriter.write(gson.toJson(json));
        bufferedWriter.close();
    }

    public void writeBoardToFile(KanBoard json) throws IOException {
        writeBoardToFile(json, json.getName());
    }

    public KanBoard readBoardFromFile(String file) throws FileNotFoundException, IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(file)));
        StringBuilder buffer = new StringBuilder();
        String str = null;
        while ((str = bufferedReader.readLine()) != null) {
            buffer.append(bufferedReader.readLine());
        }
        return gson.fromJson(buffer.toString(), KanBoard.class);
    }
}
